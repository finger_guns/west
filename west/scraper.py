import urllib.request
import http
import json


class AccuScraper(object):
    def __init__(self) -> None:
        self.API_KEY: str = 'guljGQQFekbNak00xi54qZUGBlk86Iry'
        self.url: str = 'http://dataservice.accuweather.com/'

    @property
    def url(self) -> str:
        """ Modifiable url attribute. """
        return self._url

    @url.setter
    def url(self, value: str) -> None:
        self._url = value

    def prepare_url(self) -> http.client.HTTPResponse:
        """ Return a useable response. """
        return urllib.request.urlopen(self.url)

    def location_key(self, country_code: str = 'AU', city: str = 'Melbourne') -> str:
        """ Return the location key filtered by a country and city code. """
        # Use a local variable where it makes sense to.
        url: str = self.url
        url += 'locations/v1/cities/{}/search?apikey={}&q={}'.format(country_code, self.API_KEY, city)
        response = urllib.request.urlopen(self.url).read()
        # This returns a list of a dict.
        data = json.loads(response)
        for d in data:
            # At this point it's a dict and can be interacted with as such.
            return d['Key']

    def five_day_forecast(self, location_key: str = '26216'):
        url: str = self.url
        url += 'forecasts/v1/daily/5day/{}?apikey={}'.format(location_key, self.API_KEY)
        return self.url
