# General Notes.
Theres really two parts of the ACCUWeather API that is needed.
All these return json objects.

## City Search Narrowed by Country Code.
This returns the file in json_examples called amazingly city_search_narrowed_by_country_code.json
### URL
http://dataservice.accuweather.com/locations/v1/cities/AU/search
http://dataservice.accuweather.com/locations/v1/cities/AU/search?apikey=%20%09guljGQQFekbNak00xi54qZUGBlk86Iry%20&q=Melbourne
Each city has a location key for example Melbourne Australia has the key value
of 26216.
## Forecast
This value can then be used to search for the forecast of that city code.
### URL
http://dataservice.accuweather.com/forecasts/v1/daily/5day/26216?apikey=guljGQQFekbNak00xi54qZUGBlk86Iry
http://dataservice.accuweather.com/forecasts/v1/daily/5day/26216?apikey=guljGQQFekbNak00xi54qZUGBlk86Iry&metric=true
http://dataservice.accuweather.com/forecasts/v1/daily/5day/26216
The metric true flag at the end of the url does exactly what it says.
