import unittest
from west.scraper import AccuScraper


class TestScraper(unittest.TestCase):
    """Test the scraper class"""

    @classmethod
    def setUpClass(cls):
        cls._scraper = AccuScraper()

    @classmethod
    def tearDown(cls):
        cls._scraper = None

    def test_location_key(self):
        self.assertEqual(self._scraper.location_key(), '26216')

    def test_fix_day_forecast(self):
        pass
